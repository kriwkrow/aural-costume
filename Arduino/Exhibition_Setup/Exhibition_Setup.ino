// NeoPixel includes
#include <Adafruit_NeoPixel.h>

// Magnetometer includes
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LSM9DS1.h>
#include <Adafruit_Sensor.h>

// We have 30 WRGB NeoPixels attached to pin 4 here
#define NPIX_PIN    4
#define NPIX_NUM    30
#define NPIX_RGBW   ((3 << 6) | (1 << 4) | (0 << 2) | (2)) // Took some time to decypher this

// Compass-related defines
#define COMP_N NPIX_B
#define COMP_W NPIX_W
#define COMP_S NPIX_G
#define COMP_E NPIX_R
#define MAG_OFFSET_X -10
#define MAG_OFFSET_Y -20

// Bluetooth-related defines.
#define BT_CMD_MODE_SUCCESS "CMD> "
#define BT_CMD_SUCCESS "AOK"
#define BT_CONN_SUCCESS "%STREAM_OPEN%"
#define BT_CONN_RETRY 5

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NPIX_NUM, NPIX_PIN, NPIX_RGBW + NEO_KHZ800);

// NeoPixel config stuff
float npix_fps = 120.0f; // How often we want to do the calculations and updates
long unsigned int npix_time_delay = (long unsigned int)(1000.0f / npix_fps); // How long in milliseconds to we want to have inbetween frames
long unsigned int npix_time_last = 0; // We use this in timing control
float npix_time_div_normal = 1000.0f; // The higher the slower the pulsing
float npix_time_div_change = 750.0f; // Use this and move up to normal whenever state changes
float npix_time_div_multiplier = 0.95f;
float npix_time_div = npix_time_div_normal;
int npix_max = 200; // Max brightness of LEDs (it can go up to 255)
int NPIX_R[] = {1, 0, 0, 0};
int NPIX_G[] = {0, 1, 0, 0};
int NPIX_B[] = {0, 0, 1, 0};
int NPIX_W[] = {0, 0, 0, 1};
int *npix_channel = NPIX_B; // Which channel or mix of channels we want to fade. Order: RGBW.

// Magnetometer config
Adafruit_LSM9DS1 lsm = Adafruit_LSM9DS1();
char comp_heading = 0;

// Bluetooth variables
String response;
bool b_cmd = false; // This is set to true when we enter BT command mode.
bool b_conn = false; // This is set to true when we connnect to remote.
int bt_retry = BT_CONN_RETRY;

void setup() {
  delay(100); // Wait and hope for Serial to appear

  Serial.begin(9600);
  Serial1.begin(115200); // We have a module connected to Serial1

  delay(10);

  // Start NeoPixels
  pixels.begin(); // This initializes the NeoPixel library.
  pixels.clear();

  // Start magnetometer
  if (!lsm.begin()) {
    Serial.println("Unable to initialize the LSM9DS1. Check wiring!");
    while (1);
  }
  Serial.println("Found LSM9DS1 9DOF Magnetometer");
  lsm.setupMag(lsm.LSM9DS1_MAGGAIN_4GAUSS);

  // Configure Bluetooth
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // Pull this high until we setup Bluetooth
  setup_bluetooth();
}

void loop() {
  npix_pulse();

  lsm.readMag();
  sensors_event_t a, m, g, temp;
  lsm.getEvent(&a, &m, &g, &temp);
  byte heading = comp_get_heading(m);

  if (heading != comp_heading && heading != 'u')  {
    comp_heading = heading;
    Serial1.write(heading);
    if (heading == 'n') {
      npix_change(COMP_N);
    } else if (heading == 'e') {
      npix_change(COMP_E);
    } else if (heading == 's') {
      npix_change(COMP_S);
    } else if (heading == 'w') {
      npix_change(COMP_W);
    }
  }
}

void npix_pulse(){
  long unsigned int time_now = millis();
  long unsigned int time_delta = time_now - npix_time_last;

  if (time_delta >= npix_time_delay) {
    int brightness = (int)( abs(cos( (float)time_now/npix_time_div )) * (float)npix_max );

    int r = npix_channel[0] * brightness;
    int g = npix_channel[1] * brightness;
    int b = npix_channel[2] * brightness;
    int w = npix_channel[3] * brightness;
    
    for(int i = 0; i < NPIX_NUM; i++){
      pixels.setPixelColor(i, pixels.Color(r,g,b,w)); // Moderately bright green color.
    }
    pixels.show();

    // Gradualy move npix_time_div towards npix_time_div_normal
    float npix_time_div_delta = npix_time_div_normal - npix_time_div;
    if (npix_time_div_delta < 0.25f && npix_time_div_delta != 0.0f) {
      npix_time_div_delta = 0.0f;
      npix_time_div = npix_time_div_normal;
    } else {
      npix_time_div_delta *= npix_time_div_multiplier;
      npix_time_div = npix_time_div_normal - npix_time_div_delta;
    }
    
    npix_time_last = time_now;
  }
}

// Change the channel and flicker
void npix_change(int *channel){
  npix_channel = channel;
  npix_time_div = npix_time_div_change;
}

byte comp_get_heading(sensors_event_t m){
  float mag_x = m.magnetic.x + MAG_OFFSET_X;
  float mag_y = m.magnetic.y + MAG_OFFSET_Y;

  float mag_atan = atan2(mag_x, mag_y);
  float mag_degs = mag_atan * (180.0 / PI) + 180;

  // Based on this W=0, N=90, E=180, S=270,
  // given that Z is pointing up.

/* This is when measurements are right on the edge
  if (mag_degs >= 45 && mag_degs < 135) {
    return 'n';
  } else if (mag_degs >= 135 && mag_degs < 225) {
    return 'e';
  } else if (mag_degs >= 225 && mag_degs < 315) {
    return 's';
  } else {
    return 'w';
  }
*/
  // Here we reduce the heading area by 10deg on both sides.
  // Meanig, instead of 90deg we check for the angle to be within 70deg of the direction.
  if (mag_degs >= 55 && mag_degs < 125) {
    return 'n';
  } else if (mag_degs >= 145 && mag_degs < 215) {
    return 'e';
  } else if (mag_degs >= 235 && mag_degs < 305) {
    return 's';
  } else if (mag_degs >= 325 && mag_degs < 350){
    return 'w';
  }

  return 'u'; // as for unknown
}

void setup_bluetooth(){
  Serial.println("Started Test_Bluetooth");
  Serial.println();
  delay(10);
  
  while (!b_cmd) {
    Serial.println("Attempting to enter BT command mode...");
    delay(10);
    Serial1.print("$$$");
    while (Serial1.available() <= 0) {
      delay(10);
      Serial1.println("---");
      delay(10);
      Serial1.print("$$$");
    }
    response = Serial1.readString();
    Serial.print("Response: "); Serial.println(response);
    if (!response.endsWith(BT_CMD_MODE_SUCCESS)) {
      Serial.println("Error: Failed to enter command mode.");
      Serial1.println("---");
      while (Serial1.available() == 0) {}
      response = Serial1.readString(); // Discard this response
      continue;
    }
    b_cmd = true;
    Serial.println("Entered command mode!");
    Serial.println();
    break;
  }

  delay(10);

  while (!b_conn && bt_retry != 0) {
    Serial.println("Connecting to remote host...");
    Serial1.println("C,0,E8EB1B61C509");
    while (Serial1.available() == 0) {
      delay(10);  
    }
    response = Serial1.readString();
    Serial.print("Response: "); Serial.println(response);
    response.trim();
    if (!response.endsWith(BT_CONN_SUCCESS)) {
      Serial.println("Error: Failed to connect.");
      Serial1.println("K,1"); // Try to disconnect just in case
      while (Serial1.available() == 0) {
        delay(10);  
      }
      response = Serial1.readString(); // Do nothing with the response.
      delay(10);
      bt_retry--;
      Serial.print("Attempts remaining: ");
      Serial.println(bt_retry);
      continue;
    }
    b_conn = true;
    Serial.println("Connected!");
    Serial.println();
    break;
  }

  delay(10);
  digitalWrite(LED_BUILTIN, LOW);
  
  Serial1.println("READY");
  delay(100);
}
