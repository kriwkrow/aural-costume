String response;

#define BT_CMD_MODE_SUCCESS "CMD> "
#define BT_CMD_SUCCESS "AOK"
#define BT_CONN_SUCCESS "%STREAM_OPEN%"

bool b_cmd = false; // This is set to true when we enter BT command mode.
bool b_conn = false; // This is set to true when we connnect to remote.

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  
  Serial.begin(9600);
  Serial1.begin(115200);
  
  delay(10);
  
  Serial.println("Started Test_Bluetooth");
  Serial.println();
  delay(10);

  while (!b_cmd) {
    Serial.println("Attempting to enter BT command mode...");
    Serial1.print("$$$");
    while (Serial1.available() == 0) {}
    response = Serial1.readString();
    Serial.print("Response: "); Serial.println(response);
    if (!response.startsWith(BT_CMD_MODE_SUCCESS)) {
      Serial.println("Error: Failed to enter command mode.");
      Serial1.println("---");
      while (Serial1.available() == 0) {}
      response = Serial1.readString(); // Discard this response
      continue;
    }
    b_cmd = true;
    Serial.println("Entered command mode!");
    Serial.println();
  }

  delay(10);

  Serial.println("Clearing BT whitelist...");
  Serial1.println("JC");
  while (Serial1.available() == 0) {}
  response = Serial1.readString();
  Serial.print("Response: "); Serial.println(response);
  if (!response.startsWith(BT_CMD_SUCCESS)) {
    Serial.println("Error: Failed to clear whitelist.");
    //while(1){}
  }
  Serial.println("BT whitelist cleared.");
  Serial.println();
  
  delay(10);

  Serial.println("Adding BT MIDI remote to whitelist...");
  Serial1.println("JA,0,E8EB1B61C509");
  while (Serial1.available() == 0) {}
  response = Serial1.readString();
  Serial.print("Response: "); Serial.println(response);
  if (!response.startsWith(BT_CMD_SUCCESS)) {
    Serial.println("Error: Failed to add host to whitelist.");
    //while(1){}
  }
  Serial.println();

  delay(10);

  Serial.println("Clearing bonding information...");
  Serial1.println("U,Z");
  while (Serial1.available() == 0) {}
  response = Serial1.readString();
  Serial.print("Response: "); Serial.println(response);
  if (!response.startsWith(BT_CMD_SUCCESS)) {
    Serial.println("Error: Failed to clear bonding information.");
    //while(1){}
  }
  Serial.println();

  delay(10);

  Serial.println("Disabling advertisement...");
  Serial1.println("Y");
  while (Serial1.available() == 0) {}
  response = Serial1.readString();
  Serial.print("Response: "); Serial.println(response);
  if (!response.startsWith(BT_CMD_SUCCESS)) {
    Serial.println("Error: Failed to disable advertisement.");
    //while(1){}
  }
  Serial.println();

  delay(10);

  while (!b_conn) {
    Serial.println("Connecting to remote host...");
    Serial1.println("C,0,E8EB1B61C509");
    while (Serial1.available() == 0) {}
    response = Serial1.readString();
    Serial.print("Response: "); Serial.println(response);
    response.trim();
    if (!response.endsWith(BT_CONN_SUCCESS)) {
      Serial.println("Error: Failed to connect.");
      Serial1.println("K,1"); // Try to disconnect just in case
      while (Serial1.available() == 0) {}
      response = Serial1.readString(); // Do nothing with the response.
      delay(10);
      continue;
    }
    b_conn = true;
    Serial.println("Connected!");
    Serial.println();
  }

  delay(10);
  digitalWrite(LED_BUILTIN, LOW);
  
  Serial1.println("Hello!");
  delay(1000);
}

void loop() {
  Serial1.println('n');
  delay(1000);
  Serial1.println('e');
  delay(1000);
  Serial1.println('s');
  delay(1000);
  Serial1.println('w');
  delay(1000);
}
