#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// We have 30 WRGB NeoPixels attached to pin 4 here
#define NPIX_PIN    4
#define NPIX_NUM    30
#define NPIX_RGBW   ((3 << 6) | (1 << 4) | (0 << 2) | (2)) // Took some time to decypher this

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NPIX_NUM, NPIX_PIN, NPIX_RGBW + NEO_KHZ800);

int delayval = 100; // delay for half a second

void setup() {
  pixels.begin(); // This initializes the NeoPixel library.
}

void loop() {
  pixels.clear();
  for(int i = 0; i < NPIX_NUM; i++){
    pixels.setPixelColor(i, pixels.Color(25,0,0,0)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }

  pixels.clear();
  for(int i = 0; i < NPIX_NUM; i++){
    pixels.setPixelColor(i, pixels.Color(0,25,0,0)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }

  pixels.clear();
  for(int i = 0; i < NPIX_NUM; i++){
    pixels.setPixelColor(i, pixels.Color(0,0,25,0)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }

  pixels.clear();
  for(int i = 0; i < NPIX_NUM; i++){
    pixels.setPixelColor(i, pixels.Color(0,0,0,25)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
}
