#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LSM9DS1.h>
#include <Adafruit_Sensor.h>  // not used in this demo but required!

#define MAG_OFFSET_X -10
#define MAG_OFFSET_Y -20

// We connect the sensor via i2c
Adafruit_LSM9DS1 lsm = Adafruit_LSM9DS1();

char heading = 0;

void setup() 
{
  Serial.begin(115200);

  while (!Serial) {
    delay(1); // will pause Zero, Leonardo, etc until serial console opens
  }
  
  Serial.println("LSM9DS1 data read demo");
  
  // Try to initialise and warn if we couldn't detect the chip
  if (!lsm.begin())
  {
    Serial.println("Oops ... unable to initialize the LSM9DS1. Check your wiring!");
    while (1);
  }
  Serial.println("Found LSM9DS1 9DOF");

  // helper to just set the default scaling we want, see above!
  lsm.setupMag(lsm.LSM9DS1_MAGGAIN_4GAUSS);
}

void loop() 
{
  lsm.readMag();  /* ask it to read in the data */ 

  /* Get a new sensor event */ 
  sensors_event_t a, m, g, temp;

  lsm.getEvent(&a, &m, &g, &temp); 
  
  float mag_x = m.magnetic.x + MAG_OFFSET_X;
  float mag_y = m.magnetic.y + MAG_OFFSET_Y;

  float mag_atan = atan2(mag_x, mag_y);
  float mag_degs = mag_atan * (180.0 / PI) + 180;

  // Based on this W=0, N=90, E=180, S=270,
  // given that Z is pointing up.

  if (mag_degs >= 45 && mag_degs < 135) {
    setHeading('n');
  } else if (mag_degs >= 135 && mag_degs < 225) {
    setHeading('e');
  } else if (mag_degs >= 225 && mag_degs < 315) {
    setHeading('s');
  } else {
    setHeading('w');
  }
  
  delay(100);
}

void setHeading (char h) {
  if (heading == h) {
    return;
  }

  heading = h;

  Serial.print("Heading ");

  if (heading == 'n') {
    Serial.println("North");
  } else if (heading == 'e') {
    Serial.println("East");
  } else if (heading == 's') {
    Serial.println("South");
  } else if (heading == 'w') {
    Serial.println("West");
  }
}
