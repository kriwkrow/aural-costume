# RN4871 Bluetooth Shield

![PCB Assembled](Images/PCB_Assembled.jpg)

This board was made as a daughter board for the main Aural Costume board to enable Bluetooth connectivity. The Microchip RN4871 module makes it possible to use Bluetooth as if it would be a serial link.

## PCB Design

The Bluetooth PCB is designed as a shield for the main Aural Costume board. It features a FTDI-compatible row of headers and a RN4871 module mode switch connected to pin `P2_0`. When pulled low, it enters firmware update or test mode. When high, it enters APP mode where the module can send and receive serial data. The module has to be reset or rebooted once the mode switch is changed. The module reads the mode pin once on startup. You can find out more about the module in terms of pinout and recommended PCB layout in the [RN4870/71 Data Sheet](https://ww1.microchip.com/downloads/aemDocuments/documents/WSG/ProductDocuments/DataSheets/RN4870-71-Bluetooth-Low-Energy-Module-DS50002489.pdf).

![PCB Schematic](Images/PCB_Schematic.png)
![PCB Layout](Images/PCB_Layout.png)

## Firmware Update

To take advantage of the latest features and avoid buggy behaviour, it is recommended to update the firmware. If you skip this step and experience weird behaviour, you should try to update the firmware.

You can download the firmware from the [Microchip RN4871 product page](https://www.microchip.com/en-us/product/RN4871#document-table). At the time of writing this the latest firmware version is 1.43. You should download two files.

- RN487x Firmware Update Tool
- Firmware (latest version)

Use a FTDI cable to connect the PCB to the computer. Make sure that you match the GND pins. **Warning!** You should put the module into the `TEST` mode using the onboard switch before connecting to the PC.

![Switch Test Mode](Images/FW_UPDATE_TestModeAnnotated.jpg)
![Cable Connection](Images/FW_UPDATE_CablesAnnotated.jpg)

> **Warning!** You should use a Windows computer for updating.

Once the files are downloaded, unzip them to a place where you can find. Then run the `isupdate.exe` program. Select the right COM port of your device and set the following parameters and hit **Connect**.

- Baudrate: 115200
- Memory type/subtype: flash/Embedded flash
- Address: 0000

![FW Update Main](Images/FW_UPDATE_Connect.PNG)

Then load the firmware which is split into 4 files. Hit the first **Browse** buton from the top. Select all four hex files and hit **Open**.

![FX Update Select Hex](Images/FW_UPDATE_SelectHex.PNG)
![FX Update Select Hex](Images/FW_UPDATE_HitOpen.PNG)

Then hit **Update** and wait a minute. The output screen should display a **End of Write Memory** message.

![FX Update Select Hex](Images/FW_UPDATE_EndWrite.jpg)

The Microchip RN4871 module should have the latest firmware installed. You can disconnect it from the computer and change the mode back to `APP` mode.

![FX Update Select Hex](Images/FW_UPDATE_AppMode.jpg)

## Testing the Module

Now you can connect it to the computer using the same FTDI cable as for the update and open up a serial monitor. I was using Arduino Serial Monitor. Do the following to verify that the module works.

1. Select the correct port under **Tools / Port** in Arduino IDE
1. Open the serial monitor via **Tools / Serial Monitor**
1. Set the Baud Rate to **115200** bits per second
1. Select the **no line ending** first
1. Type **$$$** in the input field and hit **Send** to enter command mode. You should see `CMD>` appearing as a response from the chip. This is a good sign.
1. **Important!** Select **Newline** in the line ending dropdown.
1. Type **D** to get some useful device information. You should see the following output.

```
CMD> BTA=E8EB1B61C509
Name=UNIQ_NAME
Connected=no
Authen=2
Features=0000
Services=C0
CMD>
```

The `UNIQ_NAME` is going to be different in your case, but let's follow the steps below to change it to whatever you want.

- Use the **SN,NEW_NAME** command to change the name of the BLE device. `NEW_NAME` can be up to 20 alphanumeric characters. You should get `AOK` as a positive response from the module.
- You need to restart the module to make the module use the name. Use the **R,1** command to initiate a complete device reboot. You should see a `Rebooting` and then `%REBOOT%` message as output.

You can also use the **---** command to exit the CMD mode. The module should respond with END in case of success.

Now the device should be discoverable with the `NEW_NAME` that you provided. Consult the [RN4871 User Guide](https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/UserGuides/RN4870-71-Bluetooth-Low-Energy-Module-User-Guide-50002466.pdf) for full overview of possible commands.

## Basic Command Reference

First things first, let's use the Arduino Serial Monitor to set the name of each of the modules.

1. Set the name of the first module to **AURAL_COS_A** (SN,AURAL_COS_A) MAC: E8EB1B61C51B
1. Set the name of the second module to **AURAL_COS_B** (SN,AURAL_COS_B) MAC: E8EB1B61C509

You can use the **GN** command to confirm that the name has been changed. You can do a hard reset using **R,1** to double-confirm.

Use the **D** command to find out the MAC address of your device.

Use the **JA,<0,1>,<MAC>** command to add the other device to the white list. Devices that are not in the whitelist will not be eligible for connection.

Use the **JB** command to add currently bonded devices to the whitelist.

Use **JD** to display the whitelist and **JC** to clear it.

Use **C,<0,1>,<address>** to connect to a device with an address specified in hex format. The first param <0,1> indicates the address type, which is 0 for public address and 1 for private random address. **B** command has to be always used after to secure the connection. Example: C,0,00A053112233. Responses: Trying, %CONNECT%, ERR, %ERR_CONN%.

You can use **C<1-8>** command to connect to one of eight stored devices.

The **B** command is used to secure the connection and bond two connected devices. This command is only effective if two devices are already connected. Once bonded, security materials are saved on both ends of the connection. Reconnection does not need authentication. Responses are: AOK, %SECURED%, %BONDED%, ERR, %ERR_SEC%

Use **U,<1-8,Z>** command to remove existing bonding. Using Z clears the bonding information. use **LB** to list existing bonded devices.

The **C** command makes the module to try to connect to last bonded device. The link is automatically secured once the connection is established. The responses are: Trying, %CONNECT%, %SECURED%, ERR

Use the **I** command to enter the UART Transparent Operation mode. It should return AOK on success.

## AURAL_COS_A Setup

We will use this as the client that will actively try to connect to AURAL_COS_B at all times.

1. Make sure "No line ending" mode is selected and send **$$$** to enter command mode of the module.
1. Clear the whitelist using **JC**. This is to avoid adding additional address to whitelist.
1. Add remote device to whitelist using **JA,0,E8EB1B61C509** where last parameter is the MAC address of the remote device. You can use **JD** to show whitelist and **JC** to clear it.
1. Clear all bonding information using **U,Z**.
1. This will be a node nobody is supposed to connect to, thus we can turn off advertisement by using command **Y**.
1. Use **C,0,E8EB1B61C509** to connect to the remote device replace the MAC with the one of your module. You can use **D** to check what it is.
1. The devices will automatically enter UART communication mode.
1. If you wish to disconnect later, enter the command mode with **$$$** and **K,1** to disconnect.


## AURAL_COS_B Setup

This configuration could be basically done only once before plugging the device into the MIDI generating Arduino.

1. Make sure "No line ending" mode is selected and send **$$$** to enter command mode of the module.
1. Clear the whitelist to avoid duplicate entries using **JC**.
1. You can add the remote device to whitelist to prevent other devices tampering with the connection. Use the **JA,0,E8EB1B61C51B** for that. Use **JD** to show whitelist and **JC** to clear it.
1. Start advertisement using **A** command. It has to be in advertisement mode so that others can connect to it.
1. Once the other device connects, UART communication mode is going to be activated automatically. It is going to print **%CONNECT,0,E8EB1B61C51B%%STREAM_OPEN%**.
