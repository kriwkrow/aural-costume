# Aural Costume

Arduino MKR Zero shield for the Aural Costume project by Juha Perä. Featuring 2 flex sensors, I2C line, I2S connection for audio amp and a Neopixel LED port with separate power connection. 

![Hero Shot](aural-cos-rev-01-hero.jpg)

## Description

This repository contains the PCB design for a Arduino MKR Zero shield to be used as part of the MA thesis that of Juha Perä.

## Usage

If you want to make it, you will need to install [KiCad](https://www.kicad.org/) and equip it with the [Fab](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) component library.

Gerber files have been generated to be compatible with [CopperCAM](https://www.galaad.net/coppercam-eng.html) software.

PCB milling was done with Roland monoFab SRM-20 milling machine. 0.2-0.5mm 60deg V bit at 0.1mm depth was used to do the isolation milling. 0.8mm end mill was used to do the drilling and cutting.

## License

Attribution-NonCommercial 2.0 Generic ([CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/)). Copyright Krisjanis Rijnieks 2022 to infinity. Made at the [Aalto Fablab](https://fablab.aalto.fi).
